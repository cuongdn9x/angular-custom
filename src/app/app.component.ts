import { NzTableSortOrder } from 'ng-zorro-antd/table';
import { Component, OnInit } from '@angular/core';
import { COL_DATA_TYPE, SortOrder } from './modules/table/models/types';
import { BehaviorSubject} from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private http: HttpClient) {}
  private apiUrl = 'https://my-json-server.typicode.com/dangngoccuong99/json-server/users'; 
  users:any[]= []

  title = 'ngvn-table';

  COL_DATA_TYPE = COL_DATA_TYPE;

  loading = false;

  sort$ = new BehaviorSubject<{ key: string, order: SortOrder }>({ key: 'name', order: null });

  totalRows =0;

  page = 1
  pageSize = 5


  handleGet(currentPage:number,limit:number) {
    const start = (currentPage - 1) * limit;
    const end = currentPage * limit;

     this.http.get(`${this.apiUrl}?_start=${start}&_end=${end}`).subscribe((data:any) => {
      this.users = data;
    });

    this.http.get(`${this.apiUrl}`).subscribe((data:any) => {
      this.totalRows = data.length;
    });
  } 
   
  ngOnInit(): void {
    this.handleGet(this.page,this.pageSize)
  }
  handleRestGet (){
    this.page = 1
    this.pageSize = 5
    this.handleGet(this.page,this.pageSize)
  }

  handleSort({ key, order }:{key:string, order:NzTableSortOrder}) {
    this.users.sort((a: any, b: any) => (a[key] < b[key]
      ? (order === 'ascend' ? 1 : -1)
      : (order === 'descend' ? -1 : 1)
    ))
  }
}
